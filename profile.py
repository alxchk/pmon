#!/usr/bin/python

import subprocess
import re
import time
import sqlite3
import string
import md5

class DataSource(object):
    header = property(lambda self      : self._get_header())
    data   = property(lambda self      : self._get_data(), 
                      lambda self,data : self._set_data(data))
    tests  = property(lambda self      : self._get_tests())

class FileSource(DataSource):
    def __init__(self, item_base, rng, base, test_values, conversion = int ):
        self._header = item_base.format('_')
        self._data   = map(lambda item : base.format(item_base.format(item)), rng) \
            if rng <> None else [base.format(item_base)]
        self._conv   = conversion
        self._test_values = test_values

    def _get_tests(self):
        return self._test_values

    def _get_header(self):
        return self._header

    def _get_data(self): 
        return max(
            map(lambda item : 
                self._conv(open(item, "r").read().rstrip()), 
                self._data))

    def _set_data(self, new_data):
        print "set {0} to {1}".format(self._header, str(new_data))
        map(lambda (item,data) :
            open(item, "w").write(str(data)), 
            zip(self._data, 
                new_data if type(new_data) == list \
                    else [new_data for _ in xrange(len(self._data))]))

class ApplicationSource(DataSource):
    def __init__(self, cmd_get, cmd_set, header, test_values, conversion = int):
        self._header = header
        self._cmd_get= cmd_get
        self._cmd_set= cmd_set
        self._conv   = conversion
        self._test_values = test_values

    def _get_tests(self):
        return self._test_values
    
    def _get_header(self):
        return self._header

    def _get_data(self):
        result = None
        try:
            result = subprocess.check_output(self._cmd_get.split(' ')).rstrip()
        finally:
            return self._conv(result)

    def _set_data(self, data):
        print "set {0} to {1}".format(self._header, str(data))
        subprocess.check_call(self._cmd_set.format(data).split(' '))


class PowerMeter:
    class DB:
        def __init__(self, name, headers):
            self._headers = headers
            self._name    = name if name<>None else self._create_db_name_from_headers()
            self._db      = sqlite3.connect(self._name)
            self._cursor  = self._db.cursor()

        def _create_db_name_from_headers(self):
            h = md5.new()
            h.update(string.join(self._headers, ','))
            return h.hexdigest() + '.db'

        def _sqlite_type(self, item):
            if type(item) == str:
                return 'STRING'
            elif type(item) == int:
                return 'INTEGER'
            else:
                return 'STR'
        
        def _create_db_from_data(self, data):
            command = "CREATE TABLE state ({0})".format(
                'id INTEGER PRIMARY KEY,' + string.join(
                    map(lambda (data, hdr) : "'" + hdr + "' " + self._sqlite_type(data), 
                        zip(data, self._headers)), ',') + ', power INTEGER')
            print command
            self._cursor.execute(command)
            
        def _append_db_with_data(self, data, powerusage, creating=False):
            if powerusage == 0:
                print "You using AC. Just do nothing"
                return

            command = "INSERT INTO state VALUES (NULL, '{0}');".format(
                string.join(map(str, data + [powerusage]), "','"))
            print command 
            
            try:
                self._cursor.execute(command)
                
            except sqlite3.OperationalError as exception:
                print "Couldn't append: " + str(exception)
                if str(exception).startswith('no such table'):
                    if not creating:
                        self._create_db_from_data(data)
                        return self._append_db_with_data(data, powerusage, True)
                                
            self._db.commit()

        def append(self, data, powerusage):
            self._append_db_with_data(data, powerusage)

    def __init__(self, meters, battery, name=None):
        self._meters = meters
        self._battery= battery
        self._db     = PowerMeter.DB(name, 
                                     map(lambda meter : meter.header, 
                                         meters))
        self._meter_to_test = 0
        self._meters_headers = None
    
    def _get_meters_data(self):
        return map(lambda meter : meter.data, self._meters)

    def _save_meters_data(self, interval=10, checks=15):
        self._db.append(map(lambda meter : meter.data, self._meters), 
                        self._check_power_usage(interval,checks))
        
    def _get_meters_header(self):
        if self._meters_headers == None:
            self._meters_headers = map(lambda meter : meter.header, self._meters)
        
        return self._meters_headers
    
    def _set_meter_value_next(self, meter):
        if meter.tests == None:
            print "No test range specified for %s" % meter.header
            return

        old_value = meter.data
        try:
            new_value = meter.tests[(list(meter.tests).index(old_value) + 1) % len(meter.tests)]
        except ValueError:
            new_value = meter.tests[0]
            pass

        print "Changing %s: %s => %s" % (meter.header, str(old_value), str(new_value))

        meter.data = new_value

    def _test_next_meter(self):
        self._set_meter_value_next(self._meters[self._meter_to_test])
        self._meter_to_test = (self._meter_to_test + 1) % len(self._meters)
        
    def _check_power_usage(self, interval=10, checks=5):
        usage_A = []
        usage_V = []
        for i in xrange(checks):
            usage_A.append(self._battery[0].data)
            usage_V.append(self._battery[0].data)
            time.sleep(interval)

        usage_A = sum(usage_A)/len(usage_A)
        usage_V = sum(usage_V)/len(usage_V)

        current = usage_A * usage_V / 100000000000

        return current

    def loop(self, **args):
        i = 0
        times          = args.get('times', True)
        test_times     = args.get('test_times', 100)
        power_interval = args.get('power_interval', 10)
        power_checks   = args.get('power_checks', 12)
        hook           = args.get('hook', lambda : None)          
        
        while times:
            self._save_meters_data(power_interval, power_checks)
            if i == 0:
                self._test_next_meter()
            i = (i + 1) % test_times
            hook()

meter = PowerMeter([
        FileSource('machinecheck{0}/check_interval', xrange(0,2),
                   '/sys/devices/system/machinecheck/{0}',
                   [0,300], int),
        ApplicationSource('/sbin/hdparm -B /dev/sda', 
                          '/sbin/hdparm -B {0} /dev/sda','HDD APM', 
                          xrange(0, 255, 5),
                          lambda data : (lambda x : 0 if x == 'off' else int(x))(
                          re.search('APM_level[ \t]+=[ \t]+([a-z0-9]+)', data).group(1))),
        FileSource('eth0', None, 
                   '/sys/class/net/{0}/operstate',
                   None, str),
        FileSource('wlan0', None, 
                   '/sys/class/net/{0}/operstate',
                   None, str),
        FileSource('acpi_video{0}', [0],
                   '/sys/class/backlight/acpi_video0/brightness', 
                   xrange(0, 15), int),
        ApplicationSource('/sbin/hdparm -a /dev/sda',
                          '/sbin/blockdev --setra {0} /dev/sda', 'HDD RA ', 
                          xrange(0, 65535, 256),
                          lambda data : 
                          int(re.search('readahead[ \t]+=[ \t]+([0-9]+)', data).group(1))),
        FileSource('dirty_ratio', None,
                   '/proc/sys/vm/{0}', 
                   xrange(0,40, 5), int),
        FileSource('dirty_background_ratio', None,
                   '/proc/sys/vm/{0}', 
                   xrange(0,80, 5), int),
        FileSource('laptop_mode', None,
                   '/proc/sys/vm/{0}', 
                   xrange(0,80, 5), int),
        ApplicationSource('/usr/bin/uptime', '/usr/bin/echo {0}', 'LA 5m', 
                          None,
                          lambda data : int(float(data.split(',')[4])*100) ),
        FileSource('sched_smt_power_savings', None,
                   '/sys/devices/system/cpu/{0}', 
                   [0,1], int),
        FileSource('snd_hda_intel', None, 
                   '/sys/module/{0}/parameters/power_save', 
                   [0,1,10], int),
        FileSource('clocksource{0}/current_clocksource', [0],
                   '/sys/devices/system/clocksource/{0}',
                   ['hpet','acpi_pm'], str),
        FileSource('machinecheck{0}/cmci_disabled', xrange(0,2),
                   '/sys/devices/system/machinecheck/{0}',
                   [0,1], int),
        FileSource('host{0}/link_power_management_policy', xrange(0,4),
                   '/sys/class/scsi_host/{0}', 
                   ['min_power', 'max_performance'], str),
        FileSource('cpu{0}/online', xrange(2,4), 
                   '/sys/devices/system/cpu/{0}', 
                   [0,1], int)],
                   
                   [
        FileSource('BAT{0}', [0],
                   '/sys/class/power_supply/{0}/current_now', int),
        FileSource('BAT{0}', [0],
                   '/sys/class/power_supply/{0}/voltage_now', int)])


print zip(meter._get_meters_header(), meter._get_meters_data())

# meter.loop(test_times=1000, power_interval=1, power_checks=1)
meter.loop()
